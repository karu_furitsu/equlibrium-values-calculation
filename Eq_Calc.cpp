//
//  Eq_Calc.cpp
//  Equilibrium_Calculation
//
//  Created by Ivan Radkevich on 5/14/20.
//  Copyright © 2020 Ivan Radkevich. All rights reserved.
//

#include "Eq_Calc.hpp"
#include <stdexcept>
#include <iostream>
#include <math.h>


double * Eq_Calc::calculate(double nuz, double nox, double nsz)
{
    double m_ref = 0;
    double m = 0;
    double b;
    try{
    if((0<=nox)&&(nox<0.45))
    {
        if(nuz<=0.9)
        {
            m_ref = 0.11;
            m = m_ref*(1+0.8)/(1+nuz);
            
        }
        else if((0.9<nuz)&&(nuz<=1.225))
        {
            m_ref = 0.0948;
            m = m_ref*(1+1.)/(1+nuz);
        }
        else if(1.225<nuz)
        {
            m_ref = 0.07176;
            m = m_ref*(1+1.45)/(1+nuz);
        }
        
    }
    else if ((0.45<=nox)and(nox<0.75))
    {
        if(nuz<=0.9)
        {
             m_ref = 0.0513;
             m = m_ref*(1+0.8)/(1+nuz);
        }
        else if((0.9<nuz)&&(nuz<=1.225))
        {
            m_ref = 0.0446;
            m = m_ref*(1+1.)/(1+nuz);
        }
        else if(1.225<nuz)
        {
            m_ref = 0.034;
            m = m_ref*(1+1.45)/(1+nuz);
        }
    }
    else if ((0.75<=nox) and (nox<1))
    {
        if(nuz<=0.9)
        {
             m_ref = 0.00615;
             m = m_ref*(1+0.8)/(1+nuz);
        }
        else if((0.9<nuz)&&(nuz<=1.225))
        {
            m_ref = 0.0048;
            m = m_ref*(1+1.)/(1+nuz);
        }
        else if(1.225<nuz)
        {
            m_ref = 0.00266;
            m = m_ref*(1+1.45)/(1+nuz);
        }
    }
    else
    {
        throw std::invalid_argument("the value of the fraction of oxygen takes non-physical value");
    }
        }
    catch(const std::invalid_argument& ia)
    {
        std::cout << "Error: " << ia.what() << std::endl;
    }
    b = N_O_ST - m;
    double N_Zr = 1.;
    double N_U = nuz;
    double N_O = 2*(nuz+nox);
    double N_FE = nsz;
    double N_tot = N_Zr + N_U + N_O + N_FE;
    double n_O = N_O/N_tot;
    double n_zr = 1./N_tot;
    double n_U = N_U/N_tot;
    double n_Fe = N_FE/N_tot;
    double y = 0.5 *(n_O + m* n_Fe - b);
    double f_y = y + sqrt(y*y+ b*m*n_Fe);
    double n_m_Fe = f_y/m;
    double n_m = n_Fe*m/f_y;
    double n_ox_O = b+ f_y;
    
    
    //now, we convert to mass fractions
    //justification : https://en.wikipedia.org/wiki/Mole_fraction#Mass_fraction
    double C_m_Fe = convert_to_mass_frac(n_m_Fe, M_Fe, 4, (1-n_m_Fe)/(1.+nuz),M_Zr,(1-n_m_Fe)*(1.-1./(1.+nuz)),M_U);
    double C_ox_O = convert_to_mass_frac(n_ox_O, M_O, 4, (1-n_ox_O)/(1.+nuz),M_Zr,(1-n_ox_O)*(1.-1./(1.+nuz)),M_U);
    double C_zr = convert_to_mass_frac(n_zr, M_Zr, 6, n_U,M_U,n_m_Fe,M_Fe,n_ox_O,M_O);
    double C_U = convert_to_mass_frac(n_U, M_U, 6, n_zr,M_Zr,n_m_Fe,M_Fe,n_ox_O,M_O);
    //justification if the following :https://en.wikipedia.org/wiki/Molar_mass#Average_molar_mass_of_mixtures
    double M_m = M_Fe*n_m_Fe+(1-n_m_Fe)/(1.+nuz)*M_Zr+(1-n_m_Fe)*(1.-1./(1.+nuz))*M_U;//metal molar mass
    double M_ox = M_O*n_ox_O+(1-n_ox_O)/(1.+nuz)*M_Zr+(1-n_ox_O)*(1.-1./(1.+nuz))*M_U;//oxide molar mass
    double C_m = convert_to_mass_frac(n_m, M_m, 2,n_O,M_ox);
    double * res = new double[9];
    res[0] = n_m_Fe;
    res[1] = n_ox_O;
    res[2] = n_m;
    res[3] = C_m_Fe;
    res[4] = C_ox_O;
    res[5] = C_m;
    res[6] = n_zr;
    res[7] = n_U;
    res[8] = C_zr;
    res[9] = C_U;
    return res;
};


double * Eq_Calc::calculate_mfstart(double C_s, double C_l, double eps_s, double n_uz)
{
    //assumption : volume = 1 m3
    
    
    double C_O = (1 - C_s)*eps_s*7000;
    double C_Fe = (1 - C_l)*(1 - eps_s)*7000;
    double C_U  =( ( C_s*eps_s*7000 + C_l*(1 - eps_s)*7000)*n_uz*(M_U/M_Zr))/(1+n_uz*(M_U/M_Zr));
    double C_Zr = ( (C_s*eps_s*7000 + C_l*(1 - eps_s)*7000))/(1+n_uz*(M_U/M_Zr));
    
    //here I don't need to multiply by 100 cuz the density is constant and it'll just factor out
    double n_O = convert_to_molar_frac(C_O, M_O, 6, C_Fe, M_Fe, C_U, M_U, C_Zr, M_Zr);
    double n_Fe = convert_to_molar_frac(C_Fe, M_Fe, 6, C_O, M_O, C_U, M_U, C_Zr, M_Zr);
    double n_U = convert_to_molar_frac(C_U, M_U, 6, C_Fe, M_Fe, C_O, M_O, C_Zr, M_Zr);
    double n_Zr = convert_to_molar_frac(C_Zr, M_Zr, 6, C_Fe, M_Fe, C_O, M_O, C_U, M_U);
   
    double N_O =  C_O/M_O;
    double N_U = C_U/M_U;
    double N_zr = C_Zr/M_Zr;
    double N_Fe = C_Fe/M_Fe;
    double n_sz = (N_Fe/N_zr);
    double n_ox = (N_O/2. - N_U)/N_zr;
    double * res = new double[9];
    res[0] = n_uz;
    res[1] = n_ox;
    res[2] = n_sz;
    res[3] = n_O;
    res[4] = n_Fe;
    res[5] = n_U;
    res[6] = n_Zr;
    return res;
};

double Eq_Calc::squareRoot(double n)
{
        
        double x = n;
        double y = 1;
        double err = 0.000001; /* acceptable error */
        while (x - y > err) {
            x = (x + y) / 2;
            y = n / x;
        }
        return x;
};

//https://en.wikipedia.org/wiki/Mole_fraction#Mass_fraction
double Eq_Calc::convert_to_mass_frac(double n, double m, int num,...)
{
    va_list valist;
    double sum = 0;
    double curr = 0;
    va_start(valist,num);
    for (int i = 0; i < num; i++)
    {
        if(i%2 == 0)
        {
            curr =  va_arg(valist, double);
        };
        if(i%2==1)
        {
        sum += curr*va_arg(valist, double);
        }
    }
    va_end(valist);
    sum += n*m;
    return n*m/sum;
};


double Eq_Calc::convert_to_molar_frac(double n, double m, int num,...)
{
    va_list valist;
    double sum = 0;
    double curr = 0;
    va_start(valist,num);
    for (int i = 0; i < num; i++)
    {
        if(i%2 == 0)
        {
            curr =  va_arg(valist, double);
        };
        if(i%2==1)
        {
        sum += curr/va_arg(valist, double);
        }
    }
    va_end(valist);
    sum += n/m;
    return (n/m)/sum;
};

double * Eq_Calc::calc_eq(double C_s, double C_l, double eps_s, double n_uz)
{
      double C_O = (1 - C_s)*eps_s*7000;
        double C_Fe = (1 - C_l)*(1 - eps_s)*7000;
        double C_U  =( ( C_s*eps_s*7000 + C_l*(1 - eps_s)*7000)*n_uz*(M_U/M_Zr))/(1+n_uz*(M_U/M_Zr));
        double C_Zr = ( (C_s*eps_s*7000 + C_l*(1 - eps_s)*7000))/(1+n_uz*(M_U/M_Zr));
        
        //here I don't need to multiply by 100 cuz the density is constant and it'll just factor out
        double n_O = convert_to_molar_frac(C_O, M_O, 6, C_Fe, M_Fe, C_U, M_U, C_Zr, M_Zr);
        double n_Fe = convert_to_molar_frac(C_Fe, M_Fe, 6, C_O, M_O, C_U, M_U, C_Zr, M_Zr);
        double n_U = convert_to_molar_frac(C_U, M_U, 6, C_Fe, M_Fe, C_O, M_O, C_Zr, M_Zr);
        double n_Zr = convert_to_molar_frac(C_Zr, M_Zr, 6, C_Fe, M_Fe, C_O, M_O, C_U, M_U);
       
        double N_O =  C_O/M_O;
        double N_U = C_U/M_U;
        double N_zr = C_Zr/M_Zr;
        double N_Fe = C_Fe/M_Fe;
        double n_sz = (N_Fe/N_zr);
        double n_ox = (N_O/2. - N_U)/N_zr;
             double nsz = (N_Fe/N_zr);
             double nox = (N_O/2. - N_U)/N_zr;
             N_O = N_O/N_zr;
             N_U = N_U/N_zr;
          double m_ref;
       double m = 0.;
          double nuz_ref = 1.;
          double b;
          try{
              if(nox<=0.3)
          {
              m_ref = 0.0948;
              m = m_ref*(1.+nuz_ref)/(1.+n_uz);
            

          }
          else if((0.3<nox)&&(nox<=0.6))
          {
              
              double m_ref1 = 0.0948;
              double m_ref2 = 0.0446;
              m_ref = m_ref1+(nox-0.3)*((m_ref2-m_ref1)/(0.3));
              m = m_ref*(1.+nuz_ref)/(1.+n_uz);
             
                
          }
          else if(nox == 0.6)
          {
              m_ref = 0.0446;
              m = m_ref*(1.+nuz_ref)/(1.+n_uz);
          }
          else if((0.6<nox)&&(nox<=0.9))
          {
              
              double m_ref1 = 0.0446;
              double m_ref2 = 0.0048;
              m_ref = m_ref1+(nox-0.6)*((m_ref2-m_ref1)/(0.3));
              m = m_ref*(1.+nuz_ref)/(1.+n_uz);
              
           
             
          }
          else if(nox>0.9)
              {
                  m_ref = 0.0048;
                  m = m_ref*(1.+nuz_ref)/(1.+n_uz);
                 
              }
          else
          {
            
              throw std::invalid_argument("the value of the fraction of oxygen takes non-physical value");
          }
              }
          catch(const std::invalid_argument& ia)
          {
              std::cout << "Error: " << ia.what() << " "<<nox<<std::endl;
          }
          b = N_O_ST - m;
      
 
          double N_tot=1+N_U+N_O+nsz;
          n_O = N_O/N_tot;
          n_Zr = 1./N_tot;
          n_U = N_U/N_tot;
          n_Fe=nsz/N_tot;
          double y = 0.5 *(n_O + m* n_Fe - b);
          double f_y = y + sqrt(y*y+ b*m*n_Fe);
          double n_m_Fe = f_y/m;
          double n_m = n_Fe*m/f_y;
          double n_ox_O = b+ f_y;
          
          
          //now, we convert to mass fractions
          //justification : https://en.wikipedia.org/wiki/Mole_fraction#Mass_fraction
       double C_m_Fe = convert_to_mass_frac(n_m_Fe, M_Fe,4, (1.-n_m_Fe)/(1.+n_uz),M_Zr,(1.-n_m_Fe)*(1.-1./(1.+n_uz)),M_U);
       double C_ox_O = convert_to_mass_frac(n_ox_O, M_O,4, (1.-n_ox_O)/(1.+n_uz),M_Zr,(1.-n_ox_O)*(1.-1./(1.+n_uz)),M_U);
          C_Zr = convert_to_mass_frac(n_Zr, M_Zr,6, n_U,M_U,n_m_Fe,M_Fe,n_ox_O,M_O);
          C_U = convert_to_mass_frac(n_U, M_U,6, n_Zr,M_Zr,n_m_Fe,M_Fe,n_ox_O,M_O);
          //justification if the following :https://en.wikipedia.org/wiki/Molar_mass#Average_molar_mass_of_mixtures
       double M_m = (M_Fe*n_m_Fe+(1.-n_m_Fe)/(1.+n_uz)*M_Zr+(1.-n_m_Fe)*(1.-1./(1.+n_uz))*M_U);//metal molar mass
       double M_ox = (M_O*n_ox_O+(1.-n_ox_O)/(1.+n_uz)*M_Zr+(1.-n_ox_O)*(1.-1./(1.+n_uz))*M_U);//oxide molar mass
       
        double C_m = convert_to_mass_frac(n_m, M_m,2,(1.-n_m),M_ox);
          
      std::cout<<" mass conservation initial composition "<<C_s*eps_s + C_l*(1.-eps_s)<<std::endl;

       std::cout<< " Cs_equi "<<1.-C_ox_O<<std::endl;

       std::cout<< " Cl_equi "<<1.-C_m_Fe<<std::endl;

       std::cout<< " eps_s_equi "<<1.-C_m<<std::endl;

       std::cout<< " mass conservation equilibrium composition"<<(1.-C_ox_O)*(1.-C_m)+(1-C_m_Fe)*C_m<<std::endl;


       double * res = new double[3];
    res[0] = 1.-C_ox_O;
    res[1] = 1.-C_m_Fe;
    res[2] = (1-C_m);
    return res;
}

