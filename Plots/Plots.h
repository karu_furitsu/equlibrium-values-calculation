//
//  Plots.h
//  Equilibrium_Calculation
//
//  Created by Ivan Radkevich on 5/26/20.
//  Copyright © 2020 Ivan Radkevich. All rights reserved.
//

#ifndef Plots_h
#define Plots_h
#define GET_VARIABLE_NAME(Variable) (#Variable)

#include <string.h>
#include <vector>
#include "Eq_Calc.hpp"
#include "gnuplot_iostream.h"



void make_plot(double nuz, double nox, double range_ll, double range_ul, int num, ...)
{
    try{
        
        
        if(range_ll>=range_ul)
        {
            throw std::invalid_argument("the upper limit is less than the lower limit");
        }
        
        Eq_Calc eq;
       
        int n = 30;
        
        double dist = (range_ul-range_ll)/n;
        
        Gnuplot gp;
        
        std::vector<std::pair<std::vector<std::pair<double, double> >, const char*> > plots;
        
        std::vector<std::string> plot_what;
        
        va_list valist;
        va_start(valist,num);
           for (int i = 0; i < num; i++)
           {
               plot_what.push_back(va_arg(valist, const char*));
               
           }
        va_end(valist);
        for(int i = 0 ; i < num ;  i++)
        {
            if(plot_what[i]=="n_m^{Fe}")
            {
                std::vector<std::pair<double, double> > xy_pts_nmFe;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                   double y = eq.calculate(nuz, nox, x)[0];
                   xy_pts_nmFe.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_nmFe,"n_m^{Fe}"));
                
            }
            else if(plot_what[i]=="n_{ox}^O")
            {
                std::vector<std::pair<double, double> > xy_pts_noxO;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                   double y = eq.calculate(nuz, nox, x)[1];
                   xy_pts_noxO.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_noxO,"n_{ox}^O"));
                
            }
            else if(plot_what[i]=="n_m")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[2];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"n_m"));
                 
            }
            else if(plot_what[i]=="n_{ox}")
            {
                std::vector<std::pair<double, double> > xy_pts_no;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = 1 - eq.calculate(nuz, nox, x)[2];
                    xy_pts_no.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_no,"n_{ox}"));
                
            }
            else if(plot_what[i]=="C_m^{Fe}")
            {
                std::vector<std::pair<double, double> > xy_pts_no;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[3];
                    xy_pts_no.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_no,"C_m^{Fe}"));
                
            }
            else if(plot_what[i]=="C_{ox}^O")
            {
                std::vector<std::pair<double, double> > xy_pts_no;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[4];
                    xy_pts_no.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_no,"C_{ox}^O"));
                
            }
            
            else if(plot_what[i]=="C_m")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[5];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"C_m"));
                 
            }
            
            else if(plot_what[i]=="n_{Zr}")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[6];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"n_{Zr}"));
                 
            }
            
            else if(plot_what[i]=="n_U")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[7];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"n_U"));
                 
            }
        
            else if(plot_what[i]=="C_{Zr}")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[8];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"C_{Zr}"));
                 
            }
            else if(plot_what[i]=="C_U")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, nox, x)[9];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"C_U"));
                 
            }
            else
            {
                 throw std::invalid_argument("no such quantity to draw");
            }
        }
        gp << "set xrange ["<<range_ll<<":"<<range_ul<<"]\nset yrange [0:1]\n";
        if(num > 2)
        {
        gp << "plot '-' with lines title '"<<plots[0].second <<"'," ;
              // '-' means read from stdin.  The send1d() function sends data to gnuplot's stdin.
        for(int i = 1; i<num-1;i++)
           {
               gp << "'-' with lines title '"<<plots[i].second <<"'," ;
           }
          gp << "'-' with lines title '"<<plots[num-1].second <<"'\n" ;
        }
        else if (num == 2)
        {
            gp << "plot '-' with lines title '"<<plots[0].second <<"'," ;
            gp << "'-' with lines title '"<<plots[1].second <<"'\n" ;
        }
        else if(num == 1)
        {
            gp << "plot '-' with lines title '"<<plots[0].second <<"'\n" ;
        }
        else
        {
            throw std::invalid_argument("number of quantities to plot error");
        }
        for(int i = 0; i<num;i++)
        {
            gp.send1d(plots[i].first);
        }
        
        
       
        std::cout << "Press enter to exit." << std::endl;
                std::cin.get();
        
        
    }
    catch(const std::invalid_argument& ia)
    {
        std::cout << "Error: " << ia.what() << std::endl;
    }
}


void make_plot_nox_var(double nuz, double nsz, double range_ll, double range_ul, int num, ...)
{
    try{
        
        
        if(range_ll>=range_ul)
        {
            throw std::invalid_argument("the upper limit is less than the lower limit");
        }
        
        Eq_Calc eq;
 
       
        int n = 30;
        
        double dist = (range_ul-range_ll)/n;
        
        Gnuplot gp;
        
        std::vector<std::pair<std::vector<std::pair<double, double> >, const char*> > plots;
        
        std::vector<std::string> plot_what;
        
        va_list valist;
        va_start(valist,num);
           for (int i = 0; i < num; i++)
           {
               plot_what.push_back(va_arg(valist, const char*));
               
           }
        va_end(valist);
        for(int i = 0 ; i < num ;  i++)
        {
            if(plot_what[i]=="n_m^{Fe}")
            {
                std::vector<std::pair<double, double> > xy_pts_nmFe;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                   double y = eq.calculate(nuz, x, nsz)[0];
                   xy_pts_nmFe.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_nmFe,"n_m^{Fe}"));
                
            }
            else if(plot_what[i]=="n_{ox}^O")
            {
                std::vector<std::pair<double, double> > xy_pts_noxO;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                   double y = eq.calculate(nuz, x, nsz)[1];
                   xy_pts_noxO.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_noxO,"n_{ox}^O"));
                
            }
            else if(plot_what[i]=="n_m")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[2];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"n_m"));
                 
            }
            else if(plot_what[i]=="n_{ox}")
            {
                std::vector<std::pair<double, double> > xy_pts_no;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = 1 - eq.calculate(nuz, x, nsz)[2];
                    xy_pts_no.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_no,"n_{ox}"));
                
            }
            else if(plot_what[i]=="C_m^{Fe}")
            {
                std::vector<std::pair<double, double> > xy_pts_no;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[3];
                    xy_pts_no.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_no,"C_m^{Fe}"));
                
            }
            else if(plot_what[i]=="C_{ox}^O")
            {
                std::vector<std::pair<double, double> > xy_pts_no;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[4];
                    xy_pts_no.push_back(std::make_pair(x, y));
                }
                plots.push_back(std::make_pair(xy_pts_no,"C_{ox}^O"));
                
            }
            
            else if(plot_what[i]=="C_m")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[5];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"C_m"));
                 
            }
            
            else if(plot_what[i]=="n_{Zr}")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[6];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"n_{Zr}"));
                 
            }
            
            else if(plot_what[i]=="n_U")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[7];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"n_U"));
                 
            }
        
            else if(plot_what[i]=="C_{Zr}")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[8];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"C_{Zr}"));
                 
            }
            else if(plot_what[i]=="C_U")
            {
                std::vector<std::pair<double, double> > xy_pts_nm;
                for(double x=range_ll; x<range_ul; x+=dist)
                {
                    double y = eq.calculate(nuz, x, nsz)[9];
                     xy_pts_nm.push_back(std::make_pair(x, y));
                }
                 plots.push_back(std::make_pair(xy_pts_nm,"C_U"));
                 
            }
            else
            {
                 throw std::invalid_argument("no such quantity to draw");
            }
        }
        gp << "set xrange ["<<range_ll<<":"<<range_ul<<"]\nset yrange [0:1]\n";
        if(num > 2)
        {
        gp << "plot '-' with lines title '"<<plots[0].second <<"'," ;
              // '-' means read from stdin.  The send1d() function sends data to gnuplot's stdin.
        for(int i = 1; i<num-1;i++)
           {
               gp << "'-' with lines title '"<<plots[i].second <<"'," ;
           }
          gp << "'-' with lines title '"<<plots[num-1].second <<"'\n" ;
        }
        else if (num == 2)
        {
            gp << "plot '-' with lines title '"<<plots[0].second <<"'," ;
            gp << "'-' with lines title '"<<plots[1].second <<"'\n" ;
        }
        else if(num == 1)
        {
            gp << "plot '-' with lines title '"<<plots[0].second <<"'\n" ;
        }
        else
        {
            throw std::invalid_argument("number of quantities to plot error");
        }
        for(int i = 0; i<num;i++)
        {
            gp.send1d(plots[i].first);
        }
        
        
       
        std::cout << "Press enter to exit." << std::endl;
                std::cin.get();
        
        
    }
    catch(const std::invalid_argument& ia)
    {
        std::cout << "Error: " << ia.what() << std::endl;
    }
}
#endif /* Plots_h */
