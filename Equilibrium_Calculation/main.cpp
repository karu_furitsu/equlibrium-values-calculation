//
//  main.cpp
//  Equilibrium_Calculation
//
//  Created by Ivan Radkevich on 5/14/20.
//  Copyright © 2020 Ivan Radkevich. All rights reserved.
//

#include <iostream>
#include "Eq_Calc.hpp"
#include "Plots.h"

int main() {
    Eq_Calc eq;
  
   /* double *res = eq.calculate(0.5, 0.9, 0.66);
    for(int i = 0;i<9;i++)
    {
        std::cout<<res[i]<<std::endl;
    }
    
    //put steps from 0 to 20 for the last parameter in eq.calculate.
    
 
    double *res1 = eq.calculate_mfstart(0.86, 0.65, 0.9, 1);
    
    for(int i = 0;i<7;i++)
    {
        std::cout<<"nuz, nox, nsz"<<res1[i]<<std::endl;
    }
    
     
    make_plot(1.0, 0.3, 0., 20., 4, "n_m^{Fe}", "n_{ox}^O", "n_m","n_{ox}");
    make_plot_nox_var(1.0, 1.4, 0., 1., 4, "n_m^{Fe}", "n_{ox}^O", "n_m","n_{ox}");
     */
    double c_s_ini = 0.8888888;
    //   double c_s_eq = 0.86;
    
    double c_l_ini = 0.;
    //   double c_l_eq = 0.65;
    
    
    double eps_s_ini = 0.9;
    //  double eps_s_eq = 0.72;
    
    for(int i = 0; i<100; i++)
    {
        double *res1 = eq.calc_eq(c_s_ini, c_l_ini, eps_s_ini, 1.147);
        c_s_ini = (res1[0]*res1[2] + c_s_ini*eps_s_ini)/(res1[2]+eps_s_ini);
        c_l_ini = (res1[1]*(1.-res1[2]) + c_l_ini*(1.-eps_s_ini))/(2-res1[2]-eps_s_ini);
        eps_s_ini = (res1[2] + eps_s_ini)/2.;
        
    }
     
     
  
    std::cout<<typeid(eq).name()<<std::endl;
    return 0;
}
