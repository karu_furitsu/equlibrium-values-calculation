//
//  Test_Eq_Calc.cpp
//  Equilibrium_Calculation
//
//  Created by Ivan Radkevich on 5/15/20.
//  Copyright © 2020 Ivan Radkevich. All rights reserved.
//

#include <stdio.h>
#include <iostream>
#include "catch.hpp"
#include "Eq_Calc.hpp"

TEST_CASE( "Eq_Calc", "Most General" )
{
    
    Eq_Calc eq;
    REQUIRE( eq.calculate(double(0.81), double(0.4), double(0.95))[0] >= 0.0);
}



//Test with iron only in the liquid  vs with U-Zr moved from Cs to Cl for the same global inventory (U should get exactly the same solution).
//we transfer 0.2 from Cs to Cl, and get Cs=0.825 and Cl = 0.445
//0.2 * 0.8 = 0.16 the global fraction of (U-Zr) that we transfer from crust to liquid
//0.16/0.2 = 0.8 the fraction of newly added (U-Zr) in the liquid (volume fraction of liquid is 0.2)
//donc, we have 0.825 Cs, 0.445 Cl: 0.16/0.36+0.001, 0.2+0.16=0.36 liquid volume fraction,
//0.64 epsilon_s : 0.8 - 0.16
TEST_CASE( "Iron only in the liquid vs half of Fe-Zr in crust moved to liquid", "Most General" )
{
    
    Eq_Calc eq;
    
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 0.8)[1] - eq.calculate_mfstart(0.825, 0.445, 0.64, 0.8)[1] < 0.0000000000000000001);
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 0.8)[2] - eq.calculate_mfstart(0.825, 0.445, 0.64, 0.8)[2] < 0.000000000000000001);
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 0.8)[3] - eq.calculate_mfstart(0.825, 0.445, 0.64, 0.8)[3] < 0.000000000000000001);
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 0.8)[4] - eq.calculate_mfstart(0.825, 0.445, 0.64, 0.8)[4] < 0.0000000000000000001);
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 0.8)[5] - eq.calculate_mfstart(0.825, 0.445, 0.64, 0.8)[5] < 0.0000000000000001);
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 0.8)[6] - eq.calculate_mfstart(0.825, 0.445, 0.64, 0.8)[6] < 0.0000000000000000001);
}

//Cuz = 1.
TEST_CASE( "Cuz = 1, n_Fe==n_Zr", "Most General" )
{
    
    Eq_Calc eq;
    
    REQUIRE( eq.calculate_mfstart(0.86, 0.001, 0.8, 1)[5] -  eq.calculate_mfstart(0.86, 0.001, 0.8, 1)[6] < 0.0000000000000001);
}




//Test with only metal
TEST_CASE( "eps_s = 0.", "Most General" )
{
    
    Eq_Calc eq;
    
    REQUIRE( eq.calculate_mfstart(0.86, 0.65, 0.001, 0.01)[3]<0.001 );
}


//Test with only oxide
TEST_CASE( "eps_s = 1.", "Most General" )
{
    
    Eq_Calc eq;
    

    REQUIRE( eq.calculate_mfstart(0.86, 0.65, 0.999, 0.8)[4] < 0.001);
}






//start with an equilibrium (U should get exactly the same solution).
//final values from CALIF3S
//Cl#M1: 0.5904
//       Cs#M1: 0.83
//       es#M1: 0.9

TEST_CASE( "Cs = , Cl = , eps_s = ", "Most General" )
{
    
    Eq_Calc eq;
    double a =  eq.calculate_mfstart(0.83, 0.5904, 0.9, 1.4)[0];
    double b =  eq.calculate_mfstart(0.83, 0.5904, 0.9, 1.4)[1];
    double c = eq.calculate_mfstart(0.83, 0.5904, 0.9, 1.4)[2];
    std::cout<<a<<" "<<b<<" "<<c<<" "<<std::endl;
    
    REQUIRE( eq.calculate(a,b, c)[6] - eq.calculate_mfstart(0.83, 0.5904, 0.9, 1.4)[6] < 0.00001);
    REQUIRE( eq.calculate(a,b, c)[7] - eq.calculate_mfstart(0.83, 0.5904, 0.9, 1.4)[5] < 0.00001);
    REQUIRE( eq.calculate(a,b, c)[1]*0.9 - eq.calculate_mfstart(0.83, 0.5904, 0.9, 1.4)[3] < 0.00001);
}

//vary Cox from minimum value (0%) to 100% increments 0.1 till 0.9 and then 0.01
