//
//  Eq_Calc.hpp
//  Equilibrium_Calculation
//
//  Created by Ivan Radkevich on 5/14/20.
//  Copyright © 2020 Ivan Radkevich. All rights reserved.
//

#ifndef Eq_Calc_hpp
#define Eq_Calc_hpp
#define N_O_ST  2/3.
#define M_Fe 55.845
#define M_Zr 91.224
#define M_O 16.
#define M_U 238.

class Eq_Calc
{
public:
    Eq_Calc() = default ;
    ~Eq_Calc() { }
    double * calculate(double nuz, double nox, double nsz);
    double * calculate_mfstart(double C_s, double C_l, double eps_s, double n_uz);
    double * calc_eq(double C_s, double C_l, double eps_s, double n_uz);
                     
    
    
   

private:
    double squareRoot(double n);
    //as we decided to work in mass fractions, we need to convert molar fractions into them
    double convert_to_mass_frac(double n, double m,int num,...);
    double convert_to_molar_frac(double n, double m,int num,...);
    

};

#endif /* Eq_Calc_hpp */
